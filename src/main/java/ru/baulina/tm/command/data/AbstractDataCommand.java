package ru.baulina.tm.command.data;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.enumerated.Role;

public abstract class AbstractDataCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN};
    }

}
