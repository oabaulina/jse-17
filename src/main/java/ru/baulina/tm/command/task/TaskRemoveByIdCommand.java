package ru.baulina.tm.command.task;

import ru.baulina.tm.entity.Task;
import ru.baulina.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    final String TASK_REMOVE_BY_ID_NAME = "task-remove-by-name";

    final String TASK_REMOVE_BY_ID_DESCRIPTION = "Remove task by name.";

    @Override
    public String name() {
        return TASK_REMOVE_BY_ID_NAME;
    }

    @Override
    public String description() {
        return TASK_REMOVE_BY_ID_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER ID");
        final Long id = TerminalUtil.nexLong();
        final Long userId = serviceLocator.getAuthService().getUserId();
        final Task task = serviceLocator.getTaskService().removeOneById(userId, id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        System.out.println("");
    }

}

