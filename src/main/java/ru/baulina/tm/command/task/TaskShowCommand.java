package ru.baulina.tm.command.task;

import ru.baulina.tm.entity.Task;

import java.util.List;

public final class TaskShowCommand extends AbstractTaskCommand {

    final String TASK_LIST_NAME = "task-list";

    final String TASK_LIST_DESCRIPTION = "Show task list.";

    @Override
    public String name() {
        return TASK_LIST_NAME;
    }

    @Override
    public String description() {
        return TASK_LIST_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[LIST TASKS]");
        final Long userId = serviceLocator.getAuthService().getUserId();
        final List<Task> tasks = serviceLocator.getTaskService().findAll(userId);
        int index = 1;
        for (Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
