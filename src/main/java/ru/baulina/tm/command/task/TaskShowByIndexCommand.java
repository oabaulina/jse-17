package ru.baulina.tm.command.task;

import ru.baulina.tm.entity.Task;
import ru.baulina.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    final String TASK_SHOW_BY_INDEX_NAME = "task-show-by-index";

    final String TASK_SHOW_BY_INDEX_DESCRIPTION = "Show task by index.";

    @Override
    public String name() {
        return TASK_SHOW_BY_INDEX_NAME;
    }

    @Override
    public String description() {
        return TASK_SHOW_BY_INDEX_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nexInt() -1;
        final Long userId = serviceLocator.getAuthService().getUserId();
        final Task task = serviceLocator.getTaskService().findOneByIndex(userId, index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
        System.out.println("");
    }

}
