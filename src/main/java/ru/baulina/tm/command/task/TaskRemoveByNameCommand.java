package ru.baulina.tm.command.task;

import ru.baulina.tm.entity.Task;
import ru.baulina.tm.util.TerminalUtil;

public final class TaskRemoveByNameCommand extends AbstractTaskCommand {

    final String TASK_REMOVE_BY_NAME_NAME = "task-remove-by-name";

    final String TASK_REMOVE_BY_NAME_DESCRIPTION = "Remove task by name.";

    @Override
    public String name() {
        return TASK_REMOVE_BY_NAME_NAME;
    }

    @Override
    public String description() {
        return TASK_REMOVE_BY_NAME_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER NAME");
        final String name = TerminalUtil.nextLine();
        final Long userId = serviceLocator.getAuthService().getUserId();
        final Task task = serviceLocator.getTaskService().removeOneByName(userId, name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        System.out.println("");
    }

}
