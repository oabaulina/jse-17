package ru.baulina.tm.command.system;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.util.SystemInformation;

public final class InfoCommand extends AbstractCommand {

    final String INFO_ARG = "-i";

    final String INFO_NAME = "info";

    final String INFO_DESCRIPTION = "Display information about system.";

    @Override
    public String arg() {
        return INFO_ARG;
    }

    @Override
    public String name() {
        return INFO_NAME;
    }

    @Override
    public String description() {
        return INFO_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[INFO]");
        System.out.println("Available processors (cores): " + SystemInformation.availableProcessors);
        System.out.println("Free memory: " + SystemInformation.freeMemoryFormat);
        System.out.println("Maximum memory: " + SystemInformation.maxMemoryFormat);
        System.out.println("Total memory available to JVM: " + SystemInformation.totalMemoryFormat);
        System.out.println("Used memory by JVM: " + SystemInformation.usedMemoryFormat);
        System.out.println("OK");
        System.out.println();
    }

}
