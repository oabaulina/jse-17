package ru.baulina.tm.command.auth;

import ru.baulina.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractAuthCommand {

    final String USER_LOCK_NAME = "locked";

    final String USER_LOCK_DESCRIPTION = "Locked user.";

    @Override
    public String name() {
        return USER_LOCK_NAME;
    }

    @Override
    public String description() {
        return USER_LOCK_DESCRIPTION;
    }

    @Override
    public void execute() {
        //serviceLocator.getAuthService().isAuth();
        System.out.println("[LOCKED_USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserLogin(login);
        System.out.println("[OK]");
        System.out.println("");
    }

}
