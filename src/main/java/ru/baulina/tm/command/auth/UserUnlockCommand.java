package ru.baulina.tm.command.auth;

import ru.baulina.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractAuthCommand {

    final String USER_UNLOCK_NAME = "unlocked";

    final String USER_UNLOCK_DESCRIPTION = "Unlocked user.";

    @Override
    public String name() {
        return USER_UNLOCK_NAME;
    }

    @Override
    public String description() {
        return USER_UNLOCK_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[LOCKED_USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().unlockUserLogin(login);
        System.out.println("[OK]");
        System.out.println("");
    }

}
