package ru.baulina.tm.command.auth;

import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.enumerated.Role;

public abstract class AbstractAuthCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN};
    }

}
