package ru.baulina.tm.command.project;

import ru.baulina.tm.entity.Project;
import ru.baulina.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    final String PROJECT_UPDATE_BY_ID_NAME = "project-update-by-id";

    final String PROJECT_UPDATE_BY_ID_DESCRIPTION = "Update project by id.";

    @Override
    public String name() {
        return PROJECT_UPDATE_BY_ID_NAME;
    }

    @Override
    public String description() {
        return PROJECT_UPDATE_BY_ID_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER ID:");
        final Long id  = TerminalUtil.nexLong();
        final Long userId = serviceLocator.getAuthService().getUserId();
        final Project project = serviceLocator.getProjectService().findOneById(userId, id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name  = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description  = TerminalUtil.nextLine();
        final Project projectUpdated = serviceLocator.getProjectService().updateTaskById(userId, id, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
        System.out.println("");
    }

}
