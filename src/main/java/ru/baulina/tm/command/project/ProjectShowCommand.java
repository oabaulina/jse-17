package ru.baulina.tm.command.project;

import ru.baulina.tm.entity.Project;

import java.util.List;

public final class ProjectShowCommand extends AbstractProjectCommand {

    final String PROJECT_LIST_NAME = "project-list";

    final String PROJECT_LIST_DESCRIPTION = "Show task project.";

    @Override
    public String name() {
        return PROJECT_LIST_NAME;
    }

    @Override
    public String description() {
        return PROJECT_LIST_DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[LIST PROJECT]");
        final Long userId = serviceLocator.getAuthService().getUserId();
        final List<Project> projects = serviceLocator.getProjectService().findAll(userId);
        int index = 1;
        for (Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
