package ru.baulina.tm.exception.system;

public class IncorrectArgumentException extends RuntimeException {

    public IncorrectArgumentException() {
        super("Error! Argument not exist...");
    }

}
