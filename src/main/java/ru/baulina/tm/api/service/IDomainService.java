package ru.baulina.tm.api.service;

import ru.baulina.tm.dto.Domain;

public interface IDomainService {

    void load(Domain domain);

    void export(Domain domain);

}
