package ru.baulina.tm.repository;

import ru.baulina.tm.api.repository.ICommandRepository;
import ru.baulina.tm.command.AbstractCommand;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public void add(final AbstractCommand command) {
        final String name = command.name();
        commands.put(name, command);
    }

    @Override
    public void remove(final AbstractCommand command) {
        final String name = command.name();
        commands.remove(name);
    }

    @Override
    public List<AbstractCommand> findAll() {
        return new ArrayList<>(commands.values());
    }

    @Override
    public List<String> getCommandsNames() {
        return new ArrayList<>(commands.keySet());
    }

    @Override
    public List<String> getArgs() {
        final List<String> Arg = new ArrayList<>();
        for (final AbstractCommand command : commands.values()) {
            if (command.arg() == null) continue;
            Arg.add(command.arg());
        }
        return Arg;
    }

    @Override
    public void clear() {
        commands.clear();
    }

    @Override
    public AbstractCommand getByArg(final String arg) {
        for (final AbstractCommand command : commands.values()) {
            if (arg.equals(command.arg())) {
                return command;
            }
        }
        return null;
    }

    @Override
    public AbstractCommand getByName(final String name) {
        return commands.get(name);
    }

    @Override
    public AbstractCommand removeByName(final String name) {
        return commands.remove(name);
    }

    @Override
    public AbstractCommand removeByArg(final String arg) {
        return null;
    }

}
